import numpy as np
import os
import re
import shutil
import time
import xml.etree.ElementTree as ET

import plotly
import plotly.graph_objs as go

def delete_folder_contents (folder_path):
    for file_object in os.listdir(folder_path):
        file_object_path = os.path.join(folder_path, file_object)
        if os.path.isfile(file_object_path):
            os.unlink(file_object_path)
        else:
            shutil.rmtree(file_object_path)

def create_video_file():
    video_file_path = '{0}{1}_NoAudio.mp4'.format(project_root_path, output_video_name)
    cmd = "{0} -r 30 -i {1}out%0{2}d.png -c:v libx264 -vf fps=30 {3}".format(ffmpeg_path, flame_folder_path, output_frame_file_num_digits, video_file_path)
    print(video_file_path)
    print(cmd)
    os.system(cmd)
    
    return video_file_path
    
def create_video_with_audio_file(video_file_path):
    video_with_audio_path = '{0}{1}_TooLong.mp4'.format(project_root_path, output_video_name)
    cmd = "{0} -i {1} -i {2} -c copy -map 0:0 -map 1:0 {3}".format(ffmpeg_path, video_file_path, input_mp3_path, video_with_audio_path)
    print(cmd)
    os.system(cmd)
    
    video_with_audio_cut_path = '{0}{1}_Final.mp4'.format(project_root_path, output_video_name)
    duration = int(frame_count / 30)
    cmd = "{0} -ss 0 -i {1} -t {2} -c copy {3}".format(ffmpeg_path, video_with_audio_path, duration, video_with_audio_cut_path)
    print(cmd)
    os.system(cmd)

def get_frame_value_sets():
    music_data_file = open(input_mp3_mfcc, 'r')
    music_data_contents = music_data_file.readlines()
    music_data_file.close()
    
    music_data = []
    for line in music_data_contents:
        row = re.split('\s|\t', line.strip(' \n'))
        music_data.append(row)

    num_value_columns = 6
    frame_value_sets = []
    for i in range(1, num_value_columns+1): #TEMP: skip the first col, since the values are weird
        frame_value_sets.append(get_frame_values(music_data, i + 1))
    
    frame_value_sets = np.transpose(frame_value_sets)

    max_val = -999999
    for value_set in frame_value_sets:
        for value in value_set:
            if value > max_val:
                max_val = value
    
    print(max_val)
    
    # Normalize values into range [-0.1, 0.1]:
    for value_set in frame_value_sets:
        for i, value in enumerate(value_set):
            value = value / max_val if max_val else 0
            value_set[i] = ((value * 2) - 1) * 0.1
    
    return frame_value_sets

def get_frame_values(music_data, value_column_index):
    frame_rate = 30
    inv_frame_rate = 1 / frame_rate
    
    frame_values = []
    cur_frame_end_seconds = inv_frame_rate
    cur_total_value = 0
    cur_count = 0
    for row in music_data:
        if len(row) < 2:
            continue
        sec = float(row[0])
        value = float(row[value_column_index])
        
        if sec >= cur_frame_end_seconds:
            avg = cur_total_value / cur_count if cur_count > 0 else 0
            frame_values.append(avg)
            cur_frame_end_seconds += inv_frame_rate
            cur_total_value = 0
            cur_count = 0
        cur_total_value += value
        cur_count += 1
    
    frame_values = frame_values[:frame_count]
    
    # Get max value:
    max_avg = -99999999
    min_avg = 9999999
    for value in frame_values:
        if value > max_avg:
            max_avg = value
        if value < min_avg:
            min_avg = value

    # Make everything start from zero
    if min_avg != 0:
        max_avg -= min_avg
        for i, value in enumerate(frame_values):
            frame_values[i] -= min_avg

    return frame_values

def adjust_sequence_values(frame_value_sets):
    delete_folder_contents(flame_folder_path)
    tree = ET.parse(input_flame_sequence)
    root = tree.getroot()

    children = []
    for child in root:
        children.append(child)

    for child in children:
        root.remove(child)

    for i in range(frame_count):
        child = children[i]

        child.attrib['quality'] = str(frame_quality)

        old_dimensions = re.split('\s', child.attrib['size'])
        old_width = float(old_dimensions[0])
        old_height = float(old_dimensions[1])
        
        old_scale = float(child.attrib['scale'])
        
        new_scale_width = (float(output_width) * old_scale) / old_width
        new_scale_height = (float(output_height) * old_scale) / old_height
        
        # Pick the smaller scale to be the new scale
        new_scale = new_scale_width if new_scale_width < new_scale_height else new_scale_height

        new_size_xy = [str(int(output_width)), str(int(output_height))]
        
        child.attrib['size'] = ' '.join(new_size_xy)
        child.attrib['scale'] = str(new_scale)
        
        flame_values = frame_value_sets[i]
        for xform in child:
            if xform.tag != 'xform':
                continue
            coefs = re.split('\s', xform.attrib['coefs'])
            adjusted_coefs = []
            for j, coef in enumerate(coefs):
                new_coef = float(coef) + flame_values[j]
                if new_coef > 1:
                    new_coef = 1
                if new_coef < -1:
                    new_coef = -1
                adjusted_coefs.append(str(new_coef))
            xform.attrib['coefs'] = ' '.join(adjusted_coefs)
        root.append(child)

    out_file_path = flame_folder_path + 'new_sequence_out.flame'
    new_tree = ET.ElementTree(root)
    new_tree.write(out_file_path)
    cmd = "cd {0} & EmberAnimate --verbose --in={1} --opencl --device=2 --prefix=out".format(fractorium_path, out_file_path)
    print(cmd)
    os.system(cmd)

def graph_frame_value_sets(frame_value_sets):
    trans_frame_value_sets = np.transpose(frame_value_sets)
    all_bar_data = []
    for j, trans_value_set in enumerate(trans_frame_value_sets):
        bar_data = go.Bar(
            x=list(range(trans_value_set.size)),
            y=trans_value_set,
            name='cat_'+str(j)
        )
        all_bar_data.append(bar_data)

    layout = go.Layout(
        barmode='stack'
    )

    fig = go.Figure(data=all_bar_data, layout=layout)
    plotly.offline.plot(fig, filename=graph_name)

project_root_path = 'D:\\Projects\\FFMusic\\'
flame_folder_path = project_root_path + 'output\\'
ffmpeg_path = project_root_path + 'ffmpeg-20161217-d8b9bef-win64-static\\bin\\ffmpeg'
fractorium_path = 'D:\\Programs\\Fractorium'

input_flame_sequence = project_root_path + 'input_sequences\\Sequence_2017-05-07-184109_900blend_7200.flame'
song_file_prefix = 'OceanDrive'
input_mp3_mfcc = project_root_path + "audio_files\\{0}_mfcc.txt".format(song_file_prefix)
input_mp3_path = project_root_path + "audio_files\\{0}.mp3".format(song_file_prefix)

output_video_name = '{0}{1}'.format(song_file_prefix, int(time.time()))
graph_name = '{0}{1}_Graph.html'.format(project_root_path, output_video_name)
output_width = 1920
output_height = 1080
frame_count = 900
frame_quality = 200

output_frame_file_num_digits = len(str(abs(frame_count-1)))

frame_value_sets = get_frame_value_sets()
graph_frame_value_sets(frame_value_sets)

adjust_sequence_values(frame_value_sets)
video_file_path = create_video_file()
create_video_with_audio_file(video_file_path)

print('Done')

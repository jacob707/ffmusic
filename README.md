# README #

This generates a fractal flame animation that goes along with a given song.  Sample output videos available at the links below.  The videos look better if you actually download and play the files from your computer, but just using the google drive preview video will give you an idea of what's going on:

- https://drive.google.com/open?id=0B9z3dbzSvTPXZGxrRHNhUld5dmc
- https://drive.google.com/open?id=0B9z3dbzSvTPXQ0FzRi1QazhRT0U

### Breakdown of the process ###

Before running the script:

- Use Fractorium (http://fractorium.com/) to generate an animation sequence of fractal flames (https://en.wikipedia.org/wiki/Fractal_flame).  All of the frames of the sequence are stored in a large text file.
- Use Aubio library to generate a text file of MFCCs (https://en.wikipedia.org/wiki/Mel-frequency_cepstrum) for a song.

The script will then do the following:

- For each frame in the fractal flame sequence, offset the flame's configuration by the MFCC value for the frame's current time.
- Build a video using the offset fractal flame sequence and the original song.

### How do I get set up? ###

* Python 3, Aubio, ffmpeg, Fractorium

### Contribution guidelines ###

* Private for now.

### Who do I talk to? ###

* Me